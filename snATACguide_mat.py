#!/usr/bin/env python

"""
create numeric guide matrix
Created by Hans-Hermann Wessels
"""

import sys
import numpy as np
from operator import itemgetter 
import pybedtools 
from itertools import islice
from argparse import ArgumentParser

parser = ArgumentParser(description='generate numeric guide matrix')
parser.add_argument('-i', '--input', help='name sorted bowtie alignment file containing reads', required=True)
parser.add_argument('-g', '--guides', help='2 column tsv file with unique guide names', required=True)
parser.add_argument('-b', '--barcodes', help='2 column tsv file with unique cell barcodes', required=True)
parser.add_argument('-o', '--output', help='string naming the output file', required=True)
options = parser.parse_args()

# input parsing
input_bwt = options.input
guide_ref = options.guides
BC_ref = options.barcodes
out = options.output

guides = {}
guides_rev = {}
barcodes = {}
barcodes_rev = {}

i = 0; j = 0;
with open(guide_ref, 'r') as fin:
    for line in fin:
        # print line.strip().split()[0]
        guides[line.strip().split()[0]] = i
        guides_rev[i] = line.strip().split()[0]
        i += 1

with open(BC_ref, 'r') as fin:
    for line in fin:
        # print line.strip().split()[0]
        barcodes[line.strip().split()[0]] = j
        barcodes_rev[j] = line.strip().split()[0]
        j += 1

mat = np.zeros((j, i))

reads = open(input_bwt, 'r')
for line in reads:

    cur_barcode = line.strip().split()[0].split(':')[0]
    cur_guide = line.strip().split()[3]

    if cur_barcode not in barcodes: sys.exit("barcode not in the list")
    if cur_guide not in guides: sys.exit("guide not in the list")

    mat[barcodes[cur_barcode], guides[cur_guide]] += 1

reads.close()  

np.savetxt(out, mat, delimiter=',', fmt="%d") 


