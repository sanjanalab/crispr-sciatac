#!/bin/bash
#$ -l mem_free=24G,h_vmem=36G
#$ -cwd
#$ -smp	pe 8

#############################
# Usage echo "bash ~/scripts/PreProcessscATAC.sh -i I1.fastq -p I2.fastq -l R1.fastq -r R2.fastq" | qsub -V -cwd -l mem_free=64G -l h_vmem=128G -pe smp 8


rflag=false
usage () {  echo -e "\n\tThis script Pre-processes scATAC index 1 and Index 2 sequencing reads for a downstream scATAC pipeline"
                        echo -e "\trun: bash PreProcessscATAC.sh -i <I1.fastq> -q <I2.fastq> -l <R1.fastq> -r <R2.fastq>"
                        echo -e "\n\tExiting! Please use the following options:\n"
                        
                        echo -e "\t    -i     Input \"I1.fastq file\" - required"
                        echo -e "\t    -q     Input \"I2.fastq file\" - required"
                        echo -e "\t    -l     Input \"R1.fastq file\" - required"
                        echo -e "\t    -r     Input \"R2.fastq file\" - required\n"; }


set -e

if (($# == 0)); then
  usage
exit 1
fi

while getopts ":i:q:l:r:h" opt; do
  case $opt in
    i)  rflag=true;I1=$OPTARG;;
    q)  rflag=true;I2=$OPTARG;;
	l)  rflag=true;R1=$OPTARG;;
	r)  rflag=true;R2=$OPTARG;;
    h)  usage; exit 1;;
    \?) usage; exit 1;;
    :)  usage; exit 1;;
  esac
done


shift $(($OPTIND - 1))

if ! $rflag && [[ -d $1 ]]
then
    usage >&2
    exit 1
fi

if [[ -z "$I1" || -z "$I2" || -z "$R1" || -z "$R2" ]]; then
    usage >&2
    exit 1
fi

#-------------------------------------------


###################
# Step 1


# load modules
module purge
module load seqkit
Threads=20
max=40000000

# combine I2 and I1 into one concatenated sequence: This only works if the order is the same, however it seems seqkit concat takes care of it 
# Like this, the first 8nt are combing from the index2 read and the subsequent bases are from I1 (first 8 of this and last 8 of this) 

# This is problematic if there are more than 50mio reads. In that case the input is split and handled seperately and concatenated afterwards.

n=$(wc -l $I2 | awk -F" " '{print $1}')


if [ "$n" -gt "$max" ]; then

    splits=$(echo $n/$max + 1| bc)


    for (( split=1; split<=${splits}; split++ ));do

        # echo $split

        tmp=$(echo $(($split-1)))
        low=$(echo $(($tmp * $max + 1))) 
        tmp=$(echo $(($split)))
        high=$(echo $(($split * $max))) 

        idx=$(printf "%03d" $split)

        echo "sed -n ${low},${high}p $I1 > I1.${idx}.fastq" >> concatlogLoop.txt
        echo "sed -n ${low},${high}p $I2 > I2.${idx}.fastq" >> concatlogLoop.txt
        echo "seqkit concat -j ${Threads} I2.${idx}.fastq I1.${idx}.fastq > IndexReads.${idx}.fastq"  >> concatlogLoop.txt

        sed -n ${low},${high}p $I1 > I1.${idx}.fastq
        sed -n ${low},${high}p $I2 > I2.${idx}.fastq

        seqkit concat -j ${Threads} I2.${idx}.fastq I1.${idx}.fastq > IndexReads.${idx}.fastq 2> concatlog.txt

        record=$(echo ${record} IndexReads.${idx}.fastq) #to ensure the order

    done

    echo "cat ${record} > IndexReads.fastq" >> concatlogLoop.txt
    echo "rm IndexReads.*.fastq I1.*.fastq I2.*.fastq" >> concatlogLoop.txt

    cat ${record} > IndexReads.fastq
    rm IndexReads.*.fastq I1.*.fastq I2.*.fastq

else
     
    seqkit concat -j ${Threads} $I2 $I1 > IndexReads.fastq 2> concatlog.txt

fi


###################
# Step 2

module purge 
module load python/2.7.8
BCcleanup='scATAC_barcode_cleaning_noAnchor.py' # no anchor sequence required
# clean up the possible barcodes 
cat IndexReads.fastq | $BCcleanup > IndexReads_cleanup.fastq 2> BCcleanupLog.txt

###################
# Step 3

# load modules
module purge
module load fastx-tools

# trim the R1 and R2 reads down to the first 30 nt 
fastx_trimmer -l 30 -i $R1 -o R1_trim.fastq
fastx_trimmer -l 30 -i $R2 -o R2_trim.fastq


###################
# Step 4

# load modules
module purge
module load seqkit


# combine the newly derived barcode read with R1 and R2 into one concatenated sequence for both mates: This only works if the order is the same, however it seems seqkit concat takes care of it 
# Like this, the first 24nt are the barcode followed by the read
# -w option just suppresses the next line width of default 60 when using Fasta, here not required as total length is < 60nt

# This is problematic if there are more than 50mio reads. In that case the input is split and handled seperately and concatenated afterwards.


if [ "$n" -gt "$max" ]; then

    splits=$(echo $n/$max + 1| bc)

    record_R1=""
    record_R2=""

    for (( split=1; split<=${splits}; split++ ));do

        # echo $split

        tmp=$(echo $(($split-1)))
        low=$(echo $(($tmp * $max + 1))) 
        tmp=$(echo $(($split)))
        high=$(echo $(($split * $max))) 

        idx=$(printf "%03d" $split)

        echo "sed -n ${low},${high}p IndexReads_cleanup.fastq > IndexReads_cleanup.${idx}.fastq" >> concatlogLoop2.txt
        echo "sed -n ${low},${high}p R1_trim.fastq > R1_trim.${idx}.fastq" >> concatlogLoop2.txt
        echo "sed -n ${low},${high}p R2_trim.fastq > R2_trim.${idx}.fastq" >> concatlogLoop2.txt
        echo "seqkit concat -j ${Threads} IndexReads_cleanup.${idx}.fastq R1_trim.${idx}.fastq > bc_R1.${idx}.fastq" >> concatlogLoop2.txt
        echo "seqkit concat -j ${Threads} IndexReads_cleanup.${idx}.fastq R2_trim.${idx}.fastq > bc_R2.${idx}.fastq" >> concatlogLoop2.txt


        sed -n ${low},${high}p IndexReads_cleanup.fastq > IndexReads_cleanup.${idx}.fastq
        sed -n ${low},${high}p R1_trim.fastq > R1_trim.${idx}.fastq
        sed -n ${low},${high}p R2_trim.fastq > R2_trim.${idx}.fastq

        seqkit concat -j ${Threads} IndexReads_cleanup.${idx}.fastq R1_trim.${idx}.fastq > bc_R1.${idx}.fastq 2> concatlog.txt
        seqkit concat -j ${Threads} IndexReads_cleanup.${idx}.fastq R2_trim.${idx}.fastq > bc_R2.${idx}.fastq 2> concatlog.txt

        record_R1=$(echo ${record_R1} bc_R1.${idx}.fastq) #to ensure the order
        record_R2=$(echo ${record_R2} bc_R2.${idx}.fastq) #to ensure the order

    done

    echo "cat ${record_R1}  > bc_R1.fastq " >> concatlogLoop2.txt
    echo "cat ${record_R2}  > bc_R2.fastq" >> concatlogLoop2.txt
    echo "rm bc_R1.*.fastq bc_R2.*.fastq IndexReads_cleanup.*.fastq R1_trim.*.fastq R2_trim.*.fastq" >> concatlogLoop2.txt

    cat ${record_R1}  > bc_R1.fastq 
    cat ${record_R2}  > bc_R2.fastq 
    rm bc_R1.*.fastq bc_R2.*.fastq IndexReads_cleanup.*.fastq R1_trim.*.fastq R2_trim.*.fastq

else
     
    seqkit concat -j ${Threads} IndexReads_cleanup.fastq R1_trim.fastq > bc_R1.fastq 2>> concatlog.txt
	seqkit concat -j ${Threads} IndexReads_cleanup.fastq R2_trim.fastq > bc_R2.fastq 2>> concatlog.txt

fi



###################
# Step 5

# Add bc to the readID and remove all reads without usable barcode
TransformReadID='UpdateReadID.py' # all read with bc == N*n with n=bc length are removed
cat bc_R1.fastq | $TransformReadID > Input_R1.fastq 
cat bc_R2.fastq | $TransformReadID > Input_R2.fastq 


############################################################ custom pre-processing end ######
#############################################################################################



#############################################################################################
####### Bing Ren Pipeline Start #############################################################



###################
# Step 6 - Alignment

# Start scATAC pipeline
module purge
module load samtools/1.1
module load python/2.7.8
module load bedtools/2.25.0
module load bowtie2/2.2.8
Idx='<GENOME_INDEX>'  # Point to genome index for Bowtie2 alignment
Threads=20


bowtie2 -D 15 -R 2 -L 22 -i S,1,1.15 -p ${Threads} -t -X2000 --no-mixed --no-discordant -x ${Idx} -1 Input_R1.fastq -2 Input_R2.fastq -S snATAC_bwt2_raw.sam 2> bwt2Log.txt
samtools view -bS snATAC_bwt2_raw.sam > snATAC_bwt2_raw.bam
samtools sort -n snATAC_bwt2_raw.bam snATAC_bwt2_nsrt
samtools sort snATAC_bwt2_raw.bam snATAC_bwt2_srt
samtools index snATAC_bwt2_srt.bam


###################
# Step 7 - Pre-processing

module load python/2.7.8 # requires pysam version > 0.8.4
# here: 
# pip install --user --upgrade --force-reinstall pysam
# pip install --user cryptography pyasn1 pyasn1-modules rsa simplejson --upgrade --force-reinstall
# cd ~/.local/lib/python2.7/site-packages
# rm -rf cryptography* pyasn1
# rm -rf cryptography* pyasn1* rsa* simplejson*
# pip list | grep pysam
# now pysam version is 0.15.1
export PATH='<PATH_TO_snATAC_BIN>' # add path to snATAC bin directory from: https://github.com/r3fang/snATAC/tree/master/bin
MAPQ=30
FLEN=2000
ELEN=75
Threads=20

snATAC pre -t ${Threads} -m ${MAPQ} -f ${FLEN} -e ${ELEN} -i snATAC_bwt2_nsrt.bam -o snATAC.inclBlackList.bed.gz 2> snATAC.pre.log


###################
# Step 7b - remove reads aligning to blacklist and scaffolds

BLACKLIST='ENCODEblacklist_hg19.bed'  # get blacklist regions file from ENCODE: https://www.encodeproject.org/ annotations/ENCSR636HFF/
bedtools subtract -a snATAC.inclBlackList.bed.gz -b ${BLACKLIST} -A | awk -F"\t" '$1 !~ "random" && $1 !~ "hap"' |gzip >  snATAC.bed.gz

###################
# Step 8 - Identify feature candidates

module load macs2/2.1.0 
Genome="hs"

# call peaks
macs2 callpeak -t snATAC.bed.gz -f BED -n snATAC -g ${Genome} -p 0.05 --nomodel --shift 150 --keep-dup all 2> snATAC.MACs2.log

#remove peaks on non-chromosoms
less snATAC_peaks.narrowPeak | awk -F"\t" '$1 !~ "random" && $1 !~ "hap"' > tmp
mv tmp snATAC_peaks.narrowPeak


###################
# Step 9 - Calculate barcode statistics


##################################################################
# calculate 3 major barcode stats
# 1) number of reads
# 2) consecutive promoter coverage 
# 3) reads in peak ratio
##################################################################
# count number of reads per barcode
zcat snATAC.bed.gz | awk '{print $4}' | sort | uniq -c | awk '{print $2, $1}' | sort -k1,1 > snATAC.reads_per_cell

# consecutive promoter coverage 
PromoterBED='PROMOTER_BEDFILE'  # Promoter locations, can be found at: http://hgdownload.cse.ucsc.edu/goldenpath/hg19/encodeDCC/wgEncodeAwgSegmentation/
intersectBed -wa -wb -a snATAC.bed.gz -b ${PromoterBED} | awk '{print $4, $8}' | sort | uniq | awk '{print $1}' | uniq -c | awk '{print $2, $1}' | sort -k1,1 > snATAC.promoter_cov

# reads in peak ratio
intersectBed -a snATAC.bed.gz -b snATAC_peaks.narrowPeak -u | awk '{print $4}' | sort | uniq -c | awk '{print $2, $1}' | sort -k1,1 - > snATAC.reads_in_peak


###################
# Step 10 - Cell selection

module load R/3.6.1

script='QC_filter.R'
PromoterBED='PROMOTER_BEDFILE'  # Promoter locations, can be found at: http://hgdownload.cse.ucsc.edu/goldenpath/hg19/encodeDCC/wgEncodeAwgSegmentation/
reads_per_barcode=500
promoter_cov_ratio=0.10
peak_cov_ratio=0.4

Rscript ${script} snATAC.reads_per_cell snATAC.promoter_cov snATAC.reads_in_peak ${PromoterBED} ${reads_per_barcode} ${promoter_cov_ratio} ${peak_cov_ratio}


###################
# Step 11 - Peak selection

script='Peak_filter.R'
PromoterBED='PROMOTER_BEDFILE'  # Promoter locations, can be found at: http://hgdownload.cse.ucsc.edu/goldenpath/hg19/encodeDCC/wgEncodeAwgSegmentation/
PeakPercentile=0.95 # keep bottom 95%
QvalCutOff=5
# top peak removal may not be needed if the alignment gets first filtered by the Blacklist

Rscript ${script} snATAC_peaks.narrowPeak ${PromoterBED} ${PeakPercentile} ${QvalCutOff}

# produces 4 different window selection - uncomment as needed
# 1 snATAC.nonPromoter.ygi 		(Bing Ren default) removed top n% (here 5%) of the peaks, and only keeps peaks that do not overlap any promotor
# 2 snATAC.Promoter.ygi			removed top n% (here 5%) of the peaks, and only keeps peaks that do overlap any promotor
# 3 snATAC.allminusTop.ygi		removed top n% (here 5%) of the peaks
# 4 snATAC.all.ygi		 		keeps all peaks

###################
# Step 12 - Generate binary accessibility matrix (NOTE: this may require large RAM)

module purge
module load samtools/1.1
module load python/2.7.8 # requires pysam version > 0.8.4
module load bedtools/2.25.0

# # snATAC bmat -i snATAC.bed.gz -x snATAC.xgi -y snATAC.nonPromoter.ygi -o snATAC.nonPromoter.mat
# # snATAC bmat -i snATAC.bed.gz -x snATAC.xgi -y snATAC.Promoter.ygi -o snATAC.Promoter.mat
# # snATAC bmat -i snATAC.bed.gz -x snATAC.xgi -y snATAC.allminusTop.ygi -o snATAC.allminusTop.mat
snATAC bmat -i snATAC.bed.gz -x snATAC.xgi -y snATAC.all.ygi -o snATAC.all.mat

# # add column and rownames
# # bash AddColRowNames.sh snATAC.nonPromoter.mat snATAC.xgi snATAC.nonPromoter.ygi
# # bash AddColRowNames.sh snATAC.Promoter.mat snATAC.xgi snATAC.Promoter.ygi
# # bash AddColRowNames.sh snATAC.allminusTop.mat snATAC.xgi snATAC.allminusTop.ygi
bash AddColRowNames.sh snATAC.all.mat snATAC.xgi snATAC.all.ygi

