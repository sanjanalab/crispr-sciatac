#!/usr/bin/env python 

################################################################################
### COPYRIGHT ##################################################################

# New York Genome Center

# SOFTWARE COPYRIGHT NOTICE AGREEMENT
# This software and its documentation are copyright (2018) by the New York
# Genome Center. All rights are reserved. This software is supplied without
# any warranty or guaranteed support whatsoever. The New York Genome Center
# cannot be responsible for its use, misuse, or functionality.

# Version: 0.1
# Author: Hans-Hermann Wessels

################################################################# /COPYRIGHT ###
################################################################################



################################################################################
### MODULES ####################################################################

from optparse import OptionParser
import sys
from itertools import islice, chain


################################################################### /MODULES ###
################################################################################



################################################################################
### FUNCTIONS ##################################################################
def fqreader(filename):
	if filename != 'stdin':
		fq = open(filename, 'r')
	else:
		fq = sys.stdin
	while True:
		r = [line.rstrip() for line in islice(fq, 4)]
		if not r:
			break
		yield r[0], r[1], r[3]
	if filename != 'stdin':
		fq.close()



################################################################# /FUNCTIONS ###
################################################################################



################################################################################
### ARGUMENTS,OPTIONS ##########################################################

parser = OptionParser(usage="\n%prog [options]", version="%prog v0.1")

parser.add_option(
	"-i",
	metavar = "FILE",
	type = "string",
	dest = "input_file",
	default = 'stdin',
	help = "Input R1 FASTQ file (default = 'stdin')"
	)

(opt, args) = parser.parse_args()

######################################################### /ARGUMENTS,OPTIONS ###
################################################################################



################################################################################
### CONSTANTS ##################################################################

bcLen=24
failseq = bcLen * 'N'

################################################################# /CONSTANTS ###
################################################################################

################################################################################
### MAIN #######################################################################

if __name__ == "__main__":

	fastq = fqreader(opt.input_file)
	for rid, seq, qual in fastq:

		bc=seq[0:bcLen]

		if bc != failseq:
		
			GenSeq=seq[bcLen:]
			GenQual=qual[bcLen:]
			StripID=rid[1:]
			NewRid=''.join(['@',bc,':',StripID])
			sys.stdout.write('\n'.join([NewRid, GenSeq, '+', GenQual]) + '\n')
		else:
			next

###################################################################### /MAIN ###
################################################################################






