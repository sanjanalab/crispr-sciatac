#!/usr/bin/env python 

################################################################################
### COPYRIGHT ##################################################################

# New York Genome Center

# SOFTWARE COPYRIGHT NOTICE AGREEMENT
# This software and its documentation are copyright (2018) by the New York
# Genome Center. All rights are reserved. This software is supplied without
# any warranty or guaranteed support whatsoever. The New York Genome Center
# cannot be responsible for its use, misuse, or functionality.

# Version: 0.1
# Author: Hans-Hermann Wessels

################################################################# /COPYRIGHT ###
################################################################################



################################################################################
### MODULES ####################################################################

from optparse import OptionParser
import sys
from itertools import islice, chain
from Bio.Seq import Seq


################################################################### /MODULES ###
################################################################################



################################################################################
### FUNCTIONS ##################################################################

def fqreader(filename):
	if filename != 'stdin':
		fq = open(filename, 'r')
	else:
		fq = sys.stdin
	while True:
		r = [line.rstrip() for line in islice(fq, 4)]
		if not r:
			break
		yield r[0], r[1], r[3]
	if filename != 'stdin':
		fq.close()

def makedegenset(seq, mmalpha):
	degenset = set()
	for i in xrange(len(seq)):
		for mm in mmalpha:
			degenset.add(seq[:i] + mm + seq[i+1:])
	return degenset

def makebcdict(bclist, mmalpha):
	bcdict = {}
	for bc in bclist:
		if len(bc)>4:
			bcdegenset = makedegenset(bc, mmalpha)
			for mbc in bcdegenset:
				bcdict[mbc] = bc
		else:
			bcdict[bc] = bc
	return bcdict

def finddegenfeature(seq, degenset, flen, minpos, maxpos):
	for i in xrange(minpos, maxpos+1):
		pfeature = seq[i:i+flen]
		if pfeature in degenset:
			return i
	return -1

def getfeatures(seq, pos, offsets, flens):
	return (seq[pos+offsets[i]:pos+offsets[i]+flens[i]] for i in xrange(len(offsets)))

def bccorrect(bc, bcdict):
	if bc in bcdict:
		return bcdict[bc]
	return bc

def makedegenset_2mm(SeqList, mmalpha):
	for x in SeqList:
		degen = set()
		full = []
		for s in x:
			degen = makedegenset(s, mmalpha)	
			full.append(degen)
		ctrlsets_2mm = (set().union(*full))
		return ctrlsets_2mm


################################################################# /FUNCTIONS ###
################################################################################



################################################################################
### ARGUMENTS,OPTIONS ##########################################################

parser = OptionParser(usage="\n%prog [options]", version="%prog v0.1")

parser.add_option(
	"-i",
	metavar = "FILE",
	type = "string",
	dest = "input_file",
	default = 'stdin',
	help = "Input R1 FASTQ file (default = 'stdin')"
	)

(opt, args) = parser.parse_args()

######################################################### /ARGUMENTS,OPTIONS ###
################################################################################



################################################################################
### CONSTANTS ##################################################################

mmalpha = ['A', 'C', 'G', 'T', 'N']

# sgRNA scaffold
l1 = 'TTTTTCAAGTTGATAACGGACTAGCCTTATTTTAACTTGCTATTTCTAGCTCTAAAAC' 

l1len = len(l1)
l1set = makedegenset(l1, mmalpha)
l1List = []
l1List.append(list(l1set))
l1set_2mm = makedegenset_2mm(l1List, mmalpha)
l1List_2mm = []
l1List_2mm.append(list(l1set_2mm))
l1set_3mm = makedegenset_2mm(l1List_2mm, mmalpha)
l1List_3mm = []
l1List_3mm.append(list(l1set_3mm))
maxl1pos = 53
minl1pos = 45


# U6 promoter; needed because some guides are 20nt some are 21 nt. Else P7i can't be matched
U6 = 'CGGTGTTTCGTCCTTTCCACAA'
U6len = len(U6)
U6set = makedegenset(U6, mmalpha)
U6List = []
U6List.append(list(U6set))
U6set_2mm = makedegenset_2mm(U6List, mmalpha)
U6List_2mm = []
U6List_2mm.append(list(U6set_2mm))
maxU6pos = 131
minU6pos = 122


# offsets to retrieve adjacent sequences
# 1. RTi at -8
# 2. P5i at -31
# 3. P7i at 100
bcoffsets = [-8, -31, 100]
bclens = [8, 8, 8]

# guide start is actually at 58, but we trim the first nt
guideoffset = [59]
# guide lengths is 19 or 20nt (20 when G is added for U6 start); However, we only record 17
guidelen = [18]



bclist_RTi = 	['TCTCGCGT','GTCGATGC','GCGGAGGA','TCTCAGGA','GGCTGCTA','CAGTATCT','CTAGTCAA','CGTATGAA',
				'GTCAACCA','GTCGCCGG','CCAATTCC','CGCCTCGG','GCGGCAAG','TCTTAGAG','AATATCGA','GCATGGTT',
				'AGAGCAGT','AGACTGAG','CCGGATTG','AGCCGTAC','GTCTGGAG','AGAACCGC','CGGAACTT','CCGAGATG',
				'GTCGCGAA','GTAAGTTG','TCCTTATT','CATTAACG','AACCTGGC','GCGCCGTT','GCAACGCC','GGTAGTCT',
				'TGGACGAG','TAATTATG','CGCCTTCT','GACGTCGT','TGACTTGG','ATAACGAG','ACCGGAGG','CTCTACTA',
				'ATTCAAGG','CTCAATTG','ATGCCTGG','TCATGCTG','GGTATCTT','GAAGAGCA','GCAGACGG','CCGCAGTC',
				'GCGCAATA','CCGGTCAG','GGCTTCAG','CGCGATTA','CGGAGCCG','AGATCTCG','CTCGGTAC','AGATTAGC',
				'GTTCGTCA','GTCCTGCC','CCTGAAGG','CGTTAGCA','AGGTCTAT','TGCAAGGA','CAAGCTAT','GCGATTGC',
				'TCAATCAG','GGAAGCGG','TCTGCATT','GCGCGCAA','TCATCGAA','TCCTCCGT','ACGAGCAG','AACGTACT',
				'CCTCCTGC','TTCTAATG','TGCAGTTG','TGGCGGTT','AAGTAATC','TATAACTC','GGCAACTG','GCTCTCTG',
				'TTGGAGAG','GCTAGAAG','GCCGGTAG','GCCGTCCG','CATCAAGC','AACGGTTC','GATGCGTT','TGGTCAGT',
				'CGTTGGTT','GACGCGCC','CCGGTTCT','GCGTCGGC','TGATTCCG','CTTGAGTT','GGACGTAG','ATGCTCAG']



bclist_P5i = 	['GGTATTGG','CGACGTTA','TTGCCGCT','GTCGTTCT','CTTCTGCG','TTGATCTG','AGTCCGCA','AACTCTCT',
				'GGAGTCTG','GTTATATG','ACGCTTAA','TTCTGGTT','CTAGATCT','ACCTTCCA','CGTTACTC','ATTCCGGT',
				'CGGTTGGT','AAGTTCGT','CCGTAGAT','CATTGCAG','CTGCAAGT','CGAGGCAG','GACGATAA','ACCTCGAG',
				'GTTCAGAA','CGCCGACC','AGAAGCCT','CGTACGTC','TCAATATA','GCCAAGAC','CTCATATT','GCGAGGTC',
				'AGAAGTTA','GAAGCAGC','TCCTCAAT','AGCCATAT','TCAGGACC','AGCCAATC','GCCTCATC','AAGTATAA',
				'TGGTATGA','CTCAGCGC','TGCCATCC','TAGTATCT','ACCAGACC','ACGCGCCT','CATAGGAT','GCCGGCAG',
				'ACCGCGTT','TCAGCGAG','GTATCTGA','CCAGAGCG','GCGCGAAC','GGATATCG','ATATCGAT','GTACGAGC',
				'CAGATATG','GAATAGGC','CATGGAAG','CCTCGACG','TATGCTAG','ATCAAGCA','CCGATCCT','CTGGATAA',
				'AACGCGCA','GATATATT','AACCGGTT','TTGGAGCT','CAGCCGTA','AACTCCGA','TGGAGTAC','CGAATATT',
				'TCCTAGGT','GGCATAGA','CTTGCGAG','CTCGCGTA','AGTACTGA','TACCGCAT','GTAGAAGT','CGCGCTCA',
				'CCAACGTT','TCCGTATA','TGGAACCG','ATAGAGAG','AACCTGCC','GCTTCCGA','CGTCCTCC','TCCAGCTC',
				'AGAGCCTA','ATAGAATC','GGTACGCC','GGTACCAA','GCGACGAT','AGTCGCGC','GAATGAGT','GTCGCCTT']


bclist_P7i = [	'TACCTACT', 'GCCTATAC', 'TCTATTCG', 'TGGCCGAG', 'TTGATCCA', 'GTAATCCG', 'AATAGACG', 'CGGCATTC', 'CTTGAGTC', 'ATATCCGC', 'CGATTAGC', 'AATACCGC', 
				'GCATCCTT', 'AAGACCAG', 'AAGCGCGG', 'GGTCAACC', 'ATCGGTTC', 'TACTTATC', 'AGAATAAG', 'AATCTCCT', 'AGACCAAT', 'CCATGCGC', 'GCGAAGCA', 'TAGTTAGG', 
				'GGCGAGCG', 'CCGTAAGT', 'GTTCAGGC', 'TCAACTTG', 'AATGAGAT', 'GCTTCTCC', 'CGGAGACT', 'AACTTGAT', 'ATCAGAGA', 'GATGGACC', 'ATTGAAGG', 'TACGAGTT', 
				'CCATATAG', 'GAGCATAC', 'AGGCCGCC', 'CGAGCAAC', 'ACGCTAAT', 'AACGAAGT', 'CCTCTCAT', 'AGACTCGT', 'GCGATACG', 'GTAGCCGG', 'TAACTAGC', 'ACGCTCGC', 
				'CAATGCTA', 'TTAGAAGT', 'GAGTTGAT', 'CCGCAACG', 'CAGTAGGA', 'ACGGCCTA', 'AACCTTCA', 'ATAGGAAG', 'ATATAAGA', 'AGAATGGT', 'CTGCGCGG', 'CCGGCGGT', 
				'CTGCGTAT', 'GGCCAGAG', 'GAGGCTGG', 'CCTGCGAG', 'CGAGTCGT', 'AGCAGAGC', 'CTTCTTAC', 'CCTGGTAT', 'ACTCTTGG', 'CTCATTGC', 'TATTATTG', 'CCGTCAGG', 
				'CATGGATT', 'CATTCGGT', 'GTATTGCG', 'GTATGAGT', 'TAGTCGTT', 'GTAGTAGA', 'GGTCGGCG', 'AATGACTT', 'CAAGGCCT', 'GCTTCTGG', 'CTTAGTTC', 'ACCTGCTT', 
				'GCGTACTT', 'TCGGTCGT', 'AGTACTTG', 'GTTGCAAC', 'AGGACTCA', 'AGACCGTT', 'CTTCATGA', 'ACTATACT', 'GATGGCCA', 'TCCTCTCA', 'GCAACTAG', 'ATCCGCGA', 
				'AATGCAAT', 'CTGATAAG', 'CGATGCGC', 'GGATATTG', 'TTGACTAT', 'GCTCTTCT', 'CTAACCGG', 'GTCTTAGC', 'CCTCAGCT', 'ATGGCGTT', 'CTCAGACG', 'CAGGTCGC', 
				'ATTCGTAG', 'TGCCTACG', 'AGCGTCTT', 'GCGTTACC', 'GTAGCGGT', 'GTCCAGGT', 'TGGACTGC', 'CCGACTCG', 'ATAGGCTT', 'TCCGATCG', 'ACCGCTTG', 'GATATGAG', 
				'TCTACTCT', 'GCAGTCAT', 'CATCCTAA', 'GAATCTGA', 'TTAACTGA', 'CCGCTAAC', 'TTAGTTGC', 'GTTCGGTT', 'GACGACTC', 'GACTGCAT', 'GTATATAT', 'TTGGCCTC', 
				'AGGCTTGC', 'GTCGGATT', 'AGGTAGTT', 'TTATCTCT', 'CTATTACT', 'TACTAAGT', 'TTGATGGT', 'ACTCGATT', 'TAACGAAG', 'AGTTGCTG', 'GTCTCTTG', 'AAGAGGAT']
bcdict_RTi = makebcdict(bclist_RTi, mmalpha)
bcdict_P5i = makebcdict(bclist_P5i, mmalpha)
bcdict_P7i = makebcdict(bclist_P7i, mmalpha)

failseq = sum(bclens) * 'N'
failqual = sum(bclens) * '#'


################################################################# /CONSTANTS ###
################################################################################

ReadCnt = 0
PassCnt = 0
FailCnt = 0
BCFailCnt = 0
L1FailCnt = 0
P5i_FailCnt = 0
RTi_FailCnt = 0
P7i_FailCnt = 0

################################################################################
### MAIN #######################################################################

if __name__ == "__main__":

	fastq = fqreader(opt.input_file)
	for rid, seq, qual in fastq:
		
		ReadCnt += 1

		# l1pos =  finddegenfeature(seq, l1set, l1len, minl1pos, maxl1pos)
		l1pos =  finddegenfeature(seq, l1set_3mm, l1len, minl1pos, maxl1pos)



		if l1pos >= 0:

			U6pos =  finddegenfeature(seq, U6set_2mm, U6len, minU6pos, maxU6pos)

			dif = U6pos - l1pos

			if dif == 77:
				bcoffsets[2] = 99
			if dif == 78:
				bcoffsets[2] = 100


			bcseqs = tuple (f for f in getfeatures(seq, l1pos, bcoffsets[0:3], bclens[0:3]))
			
			bcfail_RTi = bcseqs[0] not in bcdict_RTi
			bcfail_P5i = bcseqs[1] not in bcdict_P5i
			bcfail_P7i = bcseqs[2] not in bcdict_P7i



			if bcfail_P5i:
				P5i_FailCnt += 1
			if bcfail_RTi:
				RTi_FailCnt += 1
			if bcfail_P7i:
				P7i_FailCnt += 1
			
			bcfail = bcfail_RTi + bcfail_P5i + bcfail_P7i
			


			if not bcfail:
				
				bcseqs_RTi = (bccorrect(bc, bcdict_RTi) for bc in getfeatures(seq, l1pos, bcoffsets[0:1], bclens[0:1]))
				bcseqs_P5i = (bccorrect(bc, bcdict_P5i) for bc in getfeatures(seq, l1pos, bcoffsets[1:2], bclens[1:2]))
				bcseqs_P7i = (bccorrect(bc, bcdict_P7i) for bc in getfeatures(seq, l1pos, bcoffsets[2:3], bclens[2:3]))

				# the RTi barcode needs to be reverse complemented
				tmp = Seq(''.join(chain(bcseqs_RTi)))
				bcseqs_RTi_rc = tmp.reverse_complement()

				concatBCseqs = ''.join(chain(bcseqs_P5i,bcseqs_RTi_rc,bcseqs_P7i)) 

				guideSeq_gen = getfeatures(seq, l1pos, guideoffset, guidelen)
				guideQual_gen = getfeatures(qual, l1pos, guideoffset, guidelen)

				guideSeq = ''.join(chain(guideSeq_gen)) 
				guideQual = ''.join(chain(guideQual_gen)) 

				# quality score not needed here
				# bcqual_RTi = getfeatures(qual, l1pos, bcoffsets[0:1], bclens[0:1]) #would need to be reversed to match the rev comp of the RTi
				# bcqual_P5i = getfeatures(qual, l1pos, bcoffsets[1:2], bclens[1:2])
				# bcqual_P7i = getfeatures(qual, l1pos, bcoffsets[2:3], bclens[2:3])
				# concatBCquals = ''.join(chain(bcqual_P5i,bcqual_RTi,bcqual_P7i))



				# Add barcode to the readID
				StripID=rid[1:]
				NewRid=''.join(['@',concatBCseqs,':',StripID])


				sys.stdout.write('\n'.join([NewRid, guideSeq, '+', guideQual]) + '\n')

				PassCnt += 1
				
			else:

				# sys.stdout.write('\n'.join([rid, failseq, '+', failqual]) + '\n')
 
				BCFailCnt += 1
				FailCnt += 1
		else:

			# sys.stdout.write('\n'.join([rid, failseq, '+', failqual]) + '\n')

			L1FailCnt += 1
			FailCnt += 1
		
	
##################################################################### /MAIN ###
###############################################################################

sys.stderr.write("\n\n########################################################################\n")
sys.stderr.write("#  Barcode Cleanup finished.  \n")
sys.stderr.write(''.join(['#        ',str(ReadCnt),' reads have been processed']) + '\n')
sys.stderr.write(''.join(['#        ',str(PassCnt),' reads passed the barcode filter']) + '\n')
sys.stderr.write(''.join(['#        ',str(FailCnt),' reads did not pass the barcode or anchor sequence filter']) + '\n')
sys.stderr.write(''.join(['#        ',str(BCFailCnt),' reads did not pass the barcode filter']) + '\n')
sys.stderr.write(''.join(['#        ',str(L1FailCnt),' reads did not pass the anchor sequence filter']) + '\n')
sys.stderr.write(''.join(['#        ',str(P5i_FailCnt),' P5i barcodes could not be assigned']) + '\n')
sys.stderr.write(''.join(['#        ',str(RTi_FailCnt),' RTi barcodes could not be assigned']) + '\n')
sys.stderr.write(''.join(['#        ',str(P7i_FailCnt),' P7i barcodes could not be assigned']) + '\n')
sys.stderr.write("########################################################################\n\n")


