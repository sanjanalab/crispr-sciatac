#!/bin/bash
#$ -l mem_free=12G,h_vmem=36G
#$ -cwd
#$ -smp    pe 4

#############################
# Usage echo "bash ProcessGuidesscATAC.sh -i sgRNA.fastq.gz" | qsub -V -cwd -l mem_free=12G -l h_vmem=36G -pe smp 4


rflag=false
usage () {  echo -e "\n\tThis script Pre-processes scATAC guide sequencing reads"
                        echo -e "\trun: bash ProcessGuidesscATAC.sh -i <sgRNA.fastq.gz>"
                        echo -e "\n\tExiting! Please use the following options:\n"
                        echo -e "\t    -i     Input \"I1.fastq.gz file\" - required\n"; }


set -e

if (($# == 0)); then
  usage
exit 1
fi

while getopts ":i:h" opt; do
  case $opt in
    i)  rflag=true;FQ=$OPTARG;;
    h)  usage; exit 1;;
    \?) usage; exit 1;;
    :)  usage; exit 1;;
  esac
done


shift $(($OPTIND - 1))

if ! $rflag && [[ -d $1 ]]
then
    usage >&2
    exit 1
fi

if [[ -z "$FQ" ]]; then
    usage >&2
    exit 1
fi

#-------------------------------------------


###################
# Step 1 - Recover barcodes, extract guide sequences and tranform FQ


# load modules
module purge
module load python/2.7.8
SCRIPT='PreProcessscATACguides.py'

zcat ${FQ}  | ${SCRIPT} > sgRNA.processed.fastq 2> sgRNA.processingLog.txt


###################
# Step 2 - Align to guide reference

module purge
module load python/2.7.8
module load bowtie/1.1.2


IDX='<IDX_sgRNA_FASTA' # point to Bowtie index of the sgRNA library fasta file
bowtie -v 1 -m 1 ${IDX} -q sgRNA.processed.fastq sgRNA.aligned 2> bowtieLog.txt



###################
# Step 3 - Build guide matrix

# isolate input from bowtie alignment
less sgRNA.aligned | cut -f 3 | sort | uniq -c | awk -F" " 'BEGIN{OFS="\t"}{print $2, $1}' > sgRNAs.txt #get unique sgRNAs
less sgRNA.aligned | cut -f 1 | awk -F":" '{print $1}' | sort | uniq -c | awk -F" " 'BEGIN{OFS="\t"}{print $2, $1}' > BCs.txt #get unique barcodes
less sgRNA.aligned | sort -k1,1 > sgRNA.aligned.sorted #sort alignement by readname

# generate guide matrix
python snATACguide_mat.py -i sgRNA.aligned.sorted -g sgRNAs.txt -b BCs.txt -o snATACguide.mat

# Get column IDs and remove the new line
less sgRNAs.txt | cut -f 1 | awk -vORS="," '1' | awk '{print $0}' | sed 's/,$//' > colnames.txt 

# paste colnames onto the matrix
cat colnames.txt snATACguide.mat > tmp.mat

# create a temporary additional line
echo -e " " > tmp.txt

#paste the new line in front of the barcodes to account for the added column IDs
less BCs.txt | cut -f 1 > BCs_tmp.txt
cat tmp.txt BCs_tmp.txt > BCs.tmp.txt

# add the new column with cell barcodes in front of the matrix as column 1
paste -d , BCs.tmp.txt tmp.mat > snATACguide.IDs.mat

# remove tmp files
rm *tmp* colnames.txt
