# CRISPR-sciATAC demultiplexing

This code repository accompanies the following manuscript:

**[Profiling the genetic determinants of chromatin accessibility with scalable single-cell CRISPR screens](https://doi.org/10.1038/s41587-021-00902-x)**

Noa Liscovitch-Brauer*, Antonino Montalbano*, Jiale Deng, Alejandro Méndez-Mancilla, Hans-Hermann Wessels, Nicholas G. Moss, Chia-Yu Kung, Akash Sookdeo, Xinyi Guo, Evan Geller, Suma Jaini, Peter Smibert and Neville E. Sanjana. _Nature Biotechnology_ (2021).

Link to the published paper: https://doi.org/10.1038/s41587-021-00902-x  (Also, _bioRxiv_ preprint: https://www.biorxiv.org/content/10.1101/2020.11.20.390971v1 ).

**Processing scATAC sequencing data:**

This script `PreProcessscATAC.sh` pre-processes scATAC index 1 and Index 2 sequencing reads for a downstream scATAC pipeline.

run: `bash ~/scripts/PreProcessscATAC.sh -i <I1.fastq> -q <I2.fastq> -l <R1.fastq> -r <R2.fastq>`

**Processing matching sgRNAs sequencing data:**

The script `ProcessGuidesscATAC.sh` pre-processes scATAC guide sequencing reads.

run: `bash ProcessGuidesscATAC.sh -i <sgRNA.fastq.gz>`

Throughout the code please make sure to change the paths to different accessory scripts as necessary. 

Questions? Please contact Noa (noalis AT gmail com) or Neville (neville AT sanjanalab org).
