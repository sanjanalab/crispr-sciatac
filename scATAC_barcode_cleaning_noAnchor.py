#!/usr/bin/env python 

################################################################################
### COPYRIGHT ##################################################################

# New York Genome Center

# SOFTWARE COPYRIGHT NOTICE AGREEMENT
# This software and its documentation are copyright (2018) by the New York
# Genome Center. All rights are reserved. This software is supplied without
# any warranty or guaranteed support whatsoever. The New York Genome Center
# cannot be responsible for its use, misuse, or functionality.

# Version: 0.1
# Author: Hans-Hermann Wessels

################################################################# /COPYRIGHT ###
################################################################################



################################################################################
### MODULES ####################################################################

from optparse import OptionParser
import sys
from itertools import islice, chain


################################################################### /MODULES ###
################################################################################



################################################################################
### FUNCTIONS ##################################################################

def fqreader(filename):
	if filename != 'stdin':
		fq = open(filename, 'r')
	else:
		fq = sys.stdin
	while True:
		r = [line.rstrip() for line in islice(fq, 4)]
		if not r:
			break
		yield r[0], r[1], r[3]
	if filename != 'stdin':
		fq.close()

def makedegenset(seq, mmalpha):
	degenset = set()
	for i in xrange(len(seq)):
		for mm in mmalpha:
			degenset.add(seq[:i] + mm + seq[i+1:])
	return degenset

def makebcdict(bclist, mmalpha):
	bcdict = {}
	for bc in bclist:
		if len(bc)>4:
			bcdegenset = makedegenset(bc, mmalpha)
			for mbc in bcdegenset:
				bcdict[mbc] = bc
		else:
			bcdict[bc] = bc
	return bcdict

def finddegenfeature(seq, degenset, flen, minpos, maxpos):
	for i in xrange(minpos, maxpos+1):
		pfeature = seq[i:i+flen]
		if pfeature in degenset:
			return i
	return -1

def getfeatures(seq, pos, offsets, flens):
	return (seq[pos+offsets[i]:pos+offsets[i]+flens[i]] for i in xrange(len(offsets)))

def bccorrect(bc, bcdict):
	if bc in bcdict:
		return bcdict[bc]
	return bc

def makedegenset_2mm(SeqList, mmalpha):
	for x in SeqList:
		degen = set()
		full = []
		for s in x:
			degen = makedegenset(s, mmalpha)	
			full.append(degen)
		ctrlsets_2mm = (set().union(*full))
		return ctrlsets_2mm


################################################################# /FUNCTIONS ###
################################################################################



################################################################################
### ARGUMENTS,OPTIONS ##########################################################

parser = OptionParser(usage="\n%prog [options]", version="%prog v0.1")

parser.add_option(
	"-i",
	metavar = "FILE",
	type = "string",
	dest = "input_file",
	default = 'stdin',
	help = "Input R1 FASTQ file (default = 'stdin')"
	)

(opt, args) = parser.parse_args()

######################################################### /ARGUMENTS,OPTIONS ###
################################################################################



################################################################################
### CONSTANTS ##################################################################

mmalpha = ['A', 'C', 'G', 'T', 'N']

l1 = 'GGACAGGGACAGCCGAGCCCACGAGAC'
l1len = len(l1)
l1set = makedegenset(l1, mmalpha)
l1List = []
l1List.append(list(l1set))
l1set_2mm = makedegenset_2mm(l1List, mmalpha)
maxl1pos = 16
minl1pos = 16

# offsets to retrieve adjacent sequences
bcoffsets = [-16, -8, 27]
bclens = [8, 8, 8]

bclist_T7i = [	'ACGCGAGA', 'GCATCGAC', 'TCCTCCGC', 'TCCTGAGA', 'TAGCAGCC', 'AGATACTG', 'TTGACTAG', 'TTCATACG',
				'TGGTTGAC', 'CCGGCGAC', 'GGAATTGG', 'CCGAGGCG', 'CTTGCCGC', 'CTCTAAGA', 'TCGATATT', 'AACCATGC', 
				'ACTGCTCT', 'CTCAGTCT', 'CAATCCGG', 'GTACGGCT', 'CTCCAGAC', 'GCGGTTCT', 'AAGTTCCG', 'CATCTCGG', 
				'TTCGCGAC', 'CAACTTAC', 'AATAAGGA', 'CGTTAATG', 'GCCAGGTT', 'AACGGCGC', 'GGCGTTGC', 'AGACTACC', 
				'CTCGTCCA', 'CATAATTA', 'AGAAGGCG', 'ACGACGTC', 'CCAAGTCA', 'CTCGTTAT', 'CCTCCGGT', 'TAGTAGAG', 
				'CCTTGAAT', 'CAATTGAG', 'CCAGGCAT', 'CAGCATGA', 'AAGATACC', 'TGCTCTTC', 'CCGTCTGC', 'GACTGCGG', 
				'TATTGCGC', 'CTGACCGG', 'CTGAAGCC', 'TAATCGCG', 'CGGCTCCG', 'CGAGATCT', 'GTACCGAG', 'GCTAATCT', 
				'TGACGAAC', 'GGCAGGAC', 'CCTTCAGG', 'TGCTAACG', 'ATAGACCT', 'TCCTTGCA', 'ATAGCTTG', 'GCAATCGC', 
				'CTGATTGA', 'CCGCTTCC', 'AATGCAGA', 'TTGCGCGC', 'TTCGATGA', 'ACGGAGGA', 'CTGCTCGT', 'AGTACGTT', 
				'GCAGGAGG', 'CATTAGAA', 'CAACTGCA', 'AACCGCCA', 'GATTACTT', 'GAGTTATA', 'CAGTTGCC', 'CAGAGAGC', 
				'CTCTCCAA', 'CTTCTAGC', 'CTACCGGC', 'CGGACGGC', 'GCTTGATG', 'GAACCGTT', 'AACGCATC', 'ACTGACCA', 
				'AACCAACG', 'GGCGCGTC', 'AGAACCGG', 'GCCGACGC', 'CGGAATCA', 'AACTCAAG', 'CTACGTCC', 'CTGAGCAT']

bclist_P5i = [	'GGTATTGG', 'CGACGTTA', 'TTGCCGCT', 'GTCGTTCT', 'CTTCTGCG', 'TTGATCTG', 'AGTCCGCA', 'AACTCTCT',
				'GGAGTCTG', 'GTTATATG', 'ACGCTTAA', 'TTCTGGTT', 'CTAGATCT', 'ACCTTCCA', 'CGTTACTC', 'ATTCCGGT',
				'CGGTTGGT', 'AAGTTCGT', 'CCGTAGAT', 'CATTGCAG', 'CTGCAAGT', 'CGAGGCAG', 'GACGATAA', 'ACCTCGAG',
				'GTTCAGAA', 'CGCCGACC', 'AGAAGCCT', 'CGTACGTC', 'TCAATATA', 'GCCAAGAC', 'CTCATATT', 'GCGAGGTC',
				'AGAAGTTA', 'GAAGCAGC', 'TCCTCAAT', 'AGCCATAT', 'TCAGGACC', 'AGCCAATC', 'GCCTCATC', 'AAGTATAA',
				'TGGTATGA', 'CTCAGCGC', 'TGCCATCC', 'TAGTATCT', 'ACCAGACC', 'ACGCGCCT', 'CATAGGAT', 'GCCGGCAG',
				'ACCGCGTT', 'TCAGCGAG', 'GTATCTGA', 'CCAGAGCG', 'GCGCGAAC', 'GGATATCG', 'ATATCGAT', 'GTACGAGC',
				'CAGATATG', 'GAATAGGC', 'CATGGAAG', 'CCTCGACG', 'TATGCTAG', 'ATCAAGCA', 'CCGATCCT', 'CTGGATAA',
				'AACGCGCA', 'GATATATT', 'AACCGGTT', 'TTGGAGCT', 'CAGCCGTA', 'AACTCCGA', 'TGGAGTAC', 'CGAATATT',
				'TCCTAGGT', 'GGCATAGA', 'CTTGCGAG', 'CTCGCGTA', 'AGTACTGA', 'TACCGCAT', 'GTAGAAGT', 'CGCGCTCA',
				'CCAACGTT', 'TCCGTATA', 'TGGAACCG', 'ATAGAGAG', 'AACCTGCC', 'GCTTCCGA', 'CGTCCTCC', 'TCCAGCTC',
				'AGAGCCTA', 'ATAGAATC', 'GGTACGCC', 'GGTACCAA', 'GCGACGAT', 'AGTCGCGC', 'GAATGAGT', 'GTCGCCTT']

bclist_P7i = [	'TACCTACT', 'GCCTATAC', 'TCTATTCG', 'TGGCCGAG', 'TTGATCCA', 'GTAATCCG', 'AATAGACG', 'CGGCATTC', 'CTTGAGTC', 'ATATCCGC', 'CGATTAGC', 'AATACCGC', 
				'GCATCCTT', 'AAGACCAG', 'AAGCGCGG', 'GGTCAACC', 'ATCGGTTC', 'TACTTATC', 'AGAATAAG', 'AATCTCCT', 'AGACCAAT', 'CCATGCGC', 'GCGAAGCA', 'TAGTTAGG', 
				'GGCGAGCG', 'CCGTAAGT', 'GTTCAGGC', 'TCAACTTG', 'AATGAGAT', 'GCTTCTCC', 'CGGAGACT', 'AACTTGAT', 'ATCAGAGA', 'GATGGACC', 'ATTGAAGG', 'TACGAGTT', 
				'CCATATAG', 'GAGCATAC', 'AGGCCGCC', 'CGAGCAAC', 'ACGCTAAT', 'AACGAAGT', 'CCTCTCAT', 'AGACTCGT', 'GCGATACG', 'GTAGCCGG', 'TAACTAGC', 'ACGCTCGC', 
				'CAATGCTA', 'TTAGAAGT', 'GAGTTGAT', 'CCGCAACG', 'CAGTAGGA', 'ACGGCCTA', 'AACCTTCA', 'ATAGGAAG', 'ATATAAGA', 'AGAATGGT', 'CTGCGCGG', 'CCGGCGGT', 
				'CTGCGTAT', 'GGCCAGAG', 'GAGGCTGG', 'CCTGCGAG', 'CGAGTCGT', 'AGCAGAGC', 'CTTCTTAC', 'CCTGGTAT', 'ACTCTTGG', 'CTCATTGC', 'TATTATTG', 'CCGTCAGG', 
				'CATGGATT', 'CATTCGGT', 'GTATTGCG', 'GTATGAGT', 'TAGTCGTT', 'GTAGTAGA', 'GGTCGGCG', 'AATGACTT', 'CAAGGCCT', 'GCTTCTGG', 'CTTAGTTC', 'ACCTGCTT', 
				'GCGTACTT', 'TCGGTCGT', 'AGTACTTG', 'GTTGCAAC', 'AGGACTCA', 'AGACCGTT', 'CTTCATGA', 'ACTATACT', 'GATGGCCA', 'TCCTCTCA', 'GCAACTAG', 'ATCCGCGA', 
				'AATGCAAT', 'CTGATAAG', 'CGATGCGC', 'GGATATTG', 'TTGACTAT', 'GCTCTTCT', 'CTAACCGG', 'GTCTTAGC', 'CCTCAGCT', 'ATGGCGTT', 'CTCAGACG', 'CAGGTCGC', 
				'ATTCGTAG', 'TGCCTACG', 'AGCGTCTT', 'GCGTTACC', 'GTAGCGGT', 'GTCCAGGT', 'TGGACTGC', 'CCGACTCG', 'ATAGGCTT', 'TCCGATCG', 'ACCGCTTG', 'GATATGAG', 
				'TCTACTCT', 'GCAGTCAT', 'CATCCTAA', 'GAATCTGA', 'TTAACTGA', 'CCGCTAAC', 'TTAGTTGC', 'GTTCGGTT', 'GACGACTC', 'GACTGCAT', 'GTATATAT', 'TTGGCCTC', 
				'AGGCTTGC', 'GTCGGATT', 'AGGTAGTT', 'TTATCTCT', 'CTATTACT', 'TACTAAGT', 'TTGATGGT', 'ACTCGATT', 'TAACGAAG', 'AGTTGCTG', 'GTCTCTTG', 'AAGAGGAT']
bcdict_T7i = makebcdict(bclist_T7i, mmalpha)
bcdict_P5i = makebcdict(bclist_P5i, mmalpha)
bcdict_P7i = makebcdict(bclist_P7i, mmalpha)

failseq = sum(bclens) * 'N'
failqual = sum(bclens) * '#'



################################################################# /CONSTANTS ###
################################################################################

ReadCnt = 0
PassCnt = 0
FailCnt = 0
BCFailCnt = 0
L1FailCnt = 0
P5i_FailCnt = 0
T7i_FailCnt = 0
P7i_FailCnt = 0

################################################################################
### MAIN #######################################################################

if __name__ == "__main__":

	fastq = fqreader(opt.input_file)
	for rid, seq, qual in fastq:
		
		ReadCnt += 1

		# l1pos =  finddegenfeature(seq, l1set, l1len, minl1pos, maxl1pos)
		# l1pos =  finddegenfeature(seq, l1set_2mm, l1len, minl1pos, maxl1pos)
		l1pos = int(16)

		if l1pos >= 0:

			

			# print l1pos

			bcseqs = tuple (f for f in getfeatures(seq, l1pos, bcoffsets[0:3], bclens[0:3]))
			
			bcfail_P5i = bcseqs[0] not in bcdict_P5i
			bcfail_T7i = bcseqs[1] not in bcdict_T7i
			bcfail_P7i = bcseqs[2] not in bcdict_P7i

			if bcfail_P5i:
				P5i_FailCnt += 1
			if bcfail_T7i:
				T7i_FailCnt += 1
			if bcfail_P7i:
				P7i_FailCnt += 1
			
			bcfail = bcfail_T7i + bcfail_P5i + bcfail_P7i

			# print bcfail
			# print bcseqs[0]
			# print bcfail_P5i
			# print bcseqs[1]
			# print bcfail_T7i
			# print bcseqs[2]
			# print bcfail_P7i
			


			if not bcfail:
				
				bcseqs_P5i = (bccorrect(bc, bcdict_P5i) for bc in getfeatures(seq, l1pos, bcoffsets[0:1], bclens[0:1]))
				bcseqs_T7i = (bccorrect(bc, bcdict_T7i) for bc in getfeatures(seq, l1pos, bcoffsets[1:2], bclens[1:2]))
				bcseqs_P7i = (bccorrect(bc, bcdict_P7i) for bc in getfeatures(seq, l1pos, bcoffsets[2:3], bclens[2:3]))

				bcqual_P5i = getfeatures(qual, l1pos, bcoffsets[0:1], bclens[0:1])
				bcqual_T7i = getfeatures(qual, l1pos, bcoffsets[1:2], bclens[1:2])
				bcqual_P7i = getfeatures(qual, l1pos, bcoffsets[2:3], bclens[2:3])

				
				concatseqs = ''.join(chain(bcseqs_P5i,bcseqs_T7i,bcseqs_P7i)) 
				concatquals = ''.join(chain(bcqual_P5i,bcqual_T7i,bcqual_P7i))

				# print seq
				# print qual
				sys.stdout.write('\n'.join([rid, concatseqs, '+', concatquals]) + '\n')

				PassCnt += 1
				
			else:

				sys.stdout.write('\n'.join([rid, failseq, '+', failqual]) + '\n')
 
				BCFailCnt += 1
				FailCnt += 1
		else:

			sys.stdout.write('\n'.join([rid, failseq, '+', failqual]) + '\n')

			L1FailCnt += 1
			FailCnt += 1
		
	
###################################################################### /MAIN ###
################################################################################

sys.stderr.write("\n\n########################################################################\n")
sys.stderr.write("#  Barcode Cleanup finished.  \n")
sys.stderr.write(''.join(['#        ',str(ReadCnt),' reads have been processed']) + '\n')
sys.stderr.write(''.join(['#        ',str(PassCnt),' reads passed the barcode filter']) + '\n')
sys.stderr.write(''.join(['#        ',str(FailCnt),' reads did not pass the barcode or anchor sequence filter']) + '\n')
sys.stderr.write(''.join(['#        ',str(BCFailCnt),' reads did not pass the barcode filter']) + '\n')
sys.stderr.write(''.join(['#        ',str(L1FailCnt),' reads did not pass the anchor sequence filter (L1 anchor is switched off)']) + '\n')
sys.stderr.write(''.join(['#        ',str(P5i_FailCnt),' P5i barcodes could not be assigned']) + '\n')
sys.stderr.write(''.join(['#        ',str(T7i_FailCnt),' T7i barcodes could not be assigned']) + '\n')
sys.stderr.write(''.join(['#        ',str(P7i_FailCnt),' P7i barcodes could not be assigned']) + '\n')
sys.stderr.write("########################################################################\n\n")



