#!/bin/bash
#$ -cwd

MAT=$1 # #file.mat
BCs=$2  #file.xgi
WINDOWS=$3 #file.ygi
PREFIX="${MAT%.*}"


# Get column IDs and remove the new line
less ${WINDOWS} | awk -F"\t" '{print $1":"$2"-"$3}'| awk -vORS="\t" '1' | awk '{print $0}' | sed 's/\t$//' > colnames.txt

# paste colnames onto the matrix
cat colnames.txt ${MAT} > tmp.mat

# create a temporary additional line
echo -e " " > tmp.txt

#paste the new line in front of the barcodes to account for the added column IDs
cat tmp.txt ${BCs} > BCs_tmp.txt

# add the new column with cell barcodes in front of the matrix as column 1
paste BCs_tmp.txt tmp.mat > ${PREFIX}.IDs.mat

#rm tmp* colnames.txt BCs_tmp.txt
